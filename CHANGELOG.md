# CHANGELOG

This project adheres to [Semantic Versioning](http://semver.org/).
Which is based on [Keep A Changelog](http://keepachangelog.com/)

## [Unreleased]

### Changed

- test: use personal docker registry

### Removed

- test: remove support debian11

## v1.0.0 - 2021-08-18

### Added

- first version
