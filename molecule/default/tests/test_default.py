import os, re
import testinfra.utils.ansible_runner

def test_packages(host):
  package = host.package('icingaweb2')
  assert package.is_installed

def test_service(host):
  service = host.service('apache2')
  assert service.is_running
  assert service.is_enabled

def test_http_access(host):
  cmd = host.run('curl -v http://127.0.0.1/icingaweb2/')
  assert cmd.succeeded
  assert 'Location: /icingaweb2/authentication/login' in cmd.stderr
